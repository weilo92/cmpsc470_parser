//Sampe
/** Description of CSXToken class
 *
 *  Base token class used to contain token's line information.
 *  Can be constructed with no parameters, both line and col information,
 *  or when passed a Position Object.
 * 
 */
class CSXToken {
	int linenum;
	int colnum;
	
	CSXToken() {

	}
	
	CSXToken(int line,int col) {
		linenum = line;
		colnum = col;
	}
	
	CSXToken(Position p) {
		linenum = p.linenum;
		colnum = p.colnum;
	}
}

/** Description of CSXIntLitToken class
 *
 *  Extended token class used to contain integer literals.
 *  
 */
class CSXIntLitToken extends CSXToken {
	int intValue;

	CSXIntLitToken(int val, Position p) {
	   super(p);
	   intValue=val; 
	}
}

/** Description of CSXFloatLitToken class
 *
 *  Extended token class used to contain float literals.
 *  
 */
class CSXFloatLitToken extends CSXToken {
	float floatValue;
	
	CSXFloatLitToken(float val, Position p) {
	   super(p);
	   floatValue=val; 
	}
}

/** Description of CSXIdentifierToken class
 *
 *  Extended token class used to contain identifier literals.
 *  
 */
class CSXIdentifierToken extends CSXToken {
	String identifierText;
	
	CSXIdentifierToken(String identifier, Position p) {
		super(p);
		identifierText = identifier;
	}
}

/** Description of CSXCharLitToken class
 *
 *  Extended token class used to contain character literals.
 *  
 */
class CSXCharLitToken extends CSXToken {
	char charValue;
	
	CSXCharLitToken(char val, Position p) {
	   super(p);
	   charValue=val; 
	}
}

/** Description of CSXStringLitToken class
 *
 *  Extended token class used to contain string literals.
 *  
 */
class CSXStringLitToken extends CSXToken {
	String strValue;
	
	CSXStringLitToken(String val, Position p) {
	   super(p);
	   strValue=val; 
	}
}

/** Description of CSXErrorToken class
 *
 *  Extended token class used to as generic container for error tokens
 *  and error messages.
 *  
 */
class CSXErrorToken extends CSXToken {
	String errorValue;

	CSXErrorToken(String val, Position p) {
		super(p);
		errorValue = val;
	}
}

/** Description of Position class
 *
 *  This class is used to track the start of tokens. 
 *  
 */
class Position {
	int  linenum; 			/* maintain this as line number current
                                 		   token was scanned on */
	int  colnum; 			/* maintain this as column number current
                                 		   token began at */
	int  line; 				/* maintain this as line number after
										   scanning current token  */
	int  col; 				/* maintain this as column number after
										   scanning current token  */
	Position()
	{
  		linenum = 1; 	
  		colnum = 1; 	
  		line = 1;  
  		col = 1;
	}

	/** Description of setpos method inside Position class 
	 *
	 *  This method is used to update the line/col information
	 *  to the current running values.
	 *  
	 * 	@return 	void
	 */
	void setpos() 
	{ // set starting position for current token
		linenum = line;
		colnum = col;
	}
} ;


/** Description of Symbol class 
 *
 *  This class is used by the scanner to return token information that is useful for the parser
 *  This class will be replaced in our parser project by the java_cup.runtime.Symbol class
 *  
 */
class Symbol { 
	public int sym;
	public CSXToken value;
	public Symbol(int tokenType, CSXToken theToken) {
		sym = tokenType;
		value = theToken;
	}
}

%%

// Start of macro creation for reserved words, case insensitive

RW_FLOAT = [fF][lL][oO][aA][tT]
RW_WHILE = [wW][hH][iI][lL][eE]
RW_BOOL = [bB][oO][oO][lL]
RW_IF = [iI][fF]
RW_CONTINUE = [cC][oO][nN][tT][iI][nN][uU][eE]
RW_FALSE = [fF][aA][lL][sS][eE]
RW_PRINT = [pP][rR][iI][nN][tT]
RW_TRUE = [tT][rR][uU][eE]
RW_VOID = [vV][oO][iI][dD]
RW_CONST = [cC][oO][nN][sS][tT]
RW_ELSE = [eE][lL][sS][eE]
RW_READ = [rR][eE][aA][dD]
RW_INT = [iI][nN][tT]
RW_CLASS = [cC][lL][aA][sS][sS]
RW_RETURN = [rR][eE][tT][uU][rR][nN]
RW_CHAR = [cC][hH][aA][rR]
RW_BREAK = [bB][rR][eE][aA][kK]

// System independent macro for end of line characters

EOL = \r|\n|\r\n

// Macros for number literals
DIGIT = [0-9]
INT = [~]?{DIGIT}+
FLOAT = ([~]?{DIGIT}+[.]{DIGIT}+)|([~]?[.]{DIGIT}+)|([~]?{DIGIT}+[.])

// Macros for non EOL whitespace
BLANK = [\040]
TAB = \t

// Macros for comments
LINE_COMMENT = [/][/][^\n\r]*{EOL}
MULTILINE_COMMENT = (##|###)(#|((([^#])+#)|([^#]))*)(##|###)

// Macros for printable character, and identifier types. And charlit
PRINTABLE = [A-z!@#$%\^&*()-_=+{}\[\]|:;<,>.?/`~0-9]
IDENTIFIER = [a-zA-Z]+([a-zA-Z_]|{DIGIT})*
BAD_IDENTIFIER = (_([A-z]|{DIGIT})*)|{DIGIT}+([A-z_])([a-zA-Z_]|{DIGIT})*
CHARLIT = {PRINTABLE}|\t

%{
	StringBuffer str = new StringBuffer();
%}

// State declarations
%state STRING
%state CHAR
%state IDSTATE

%type Symbol
%eofval{
  return new Symbol(sym.EOF, new CSXToken(0,0));
%eofval}
%{
Position Pos = new Position();
%}

%%
/***********************************************************************
 Tokens for the CSX language are defined here using regular expressions
************************************************************************/

// Tokens to match against during YYINITIAL state.

<YYINITIAL> {
	{RW_BOOL} {
		Pos.setpos();
		Pos.col +=4;
		return new Symbol(sym.rw_BOOL, new CSXToken(Pos));
	}

	{RW_CONST} {
		Pos.setpos();
		Pos.col +=5;
		return new Symbol(sym.rw_CONST,	new CSXToken(Pos));
	}

	{RW_FLOAT} {
		Pos.setpos();
		Pos.col +=5;
		return new Symbol(sym.rw_FLOAT,	new CSXToken(Pos));
	}

	{RW_INT} {
		Pos.setpos();
		Pos.col +=3;
		return new Symbol(sym.rw_INT, new CSXToken(Pos));
	}

	{RW_WHILE} {
		Pos.setpos();
		Pos.col +=5;
		return new Symbol(sym.rw_WHILE,	new CSXToken(Pos));
	}

	{RW_IF} {
		Pos.setpos();
		Pos.col +=2;
		return new Symbol(sym.rw_IF, new CSXToken(Pos));
	}

	{RW_CONTINUE} {
		Pos.setpos();
		Pos.col +=8;
		return new Symbol(sym.rw_CONTINUE, new CSXToken(Pos));
	}

	{RW_FALSE} {
		Pos.setpos();
		Pos.col +=5;
		return new Symbol(sym.rw_FALSE, new CSXToken(Pos));
	}

	{RW_PRINT} {
		Pos.setpos();
		Pos.col +=5;
		return new Symbol(sym.rw_PRINT, new CSXToken(Pos));
	}

	{RW_TRUE} {
		Pos.setpos();
		Pos.col +=4;
		return new Symbol(sym.rw_TRUE, new CSXToken(Pos));
	}

	{RW_VOID} {
		Pos.setpos();
		Pos.col +=4;
		return new Symbol(sym.rw_VOID, new CSXToken(Pos));
	}

	{RW_ELSE} {
		Pos.setpos();
		Pos.col +=4;
		return new Symbol(sym.rw_ELSE, new CSXToken(Pos));
	}

	{RW_READ} {
		Pos.setpos();
		Pos.col +=4;
		return new Symbol(sym.rw_READ, new CSXToken(Pos));
	}

	{RW_CLASS} {
		Pos.setpos();
		Pos.col +=5;
		return new Symbol(sym.rw_CLASS, new CSXToken(Pos));
	}

	{RW_RETURN} {
		Pos.setpos();
		Pos.col +=6;
		return new Symbol(sym.rw_RETURN, new CSXToken(Pos));
	}

	{RW_CHAR} {
		Pos.setpos();
		Pos.col +=4;
		return new Symbol(sym.rw_CHAR, new CSXToken(Pos));
	}

	{RW_BREAK} {
		Pos.setpos();
		Pos.col +=5;
		return new Symbol(sym.rw_BREAK, new CSXToken(Pos));
	}

	"+" {
		Pos.setpos();
		Pos.col += 1;
		return new Symbol(sym.PLUS, new CSXToken(Pos));
	}

	"-" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.MINUS, new CSXToken(Pos));
	}

	"*" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.TIMES, new CSXToken(Pos));
	}

	"{" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.LBRACE, new CSXToken(Pos));
	}

	"}" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.RBRACE, new CSXToken(Pos));
	}

	"(" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.LPAREN, new CSXToken(Pos));
	}

	")" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.RPAREN, new CSXToken(Pos));
	}

	"[" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.LBRACKET, new CSXToken(Pos));
	}

	"]" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.RBRACKET, new CSXToken(Pos));
	}

	{LINE_COMMENT} {
		Pos.setpos();
		Pos.line += 1;
		Pos.col = 1;
	}

	{MULTILINE_COMMENT} {
		int newLines = yytext().split(System.lineSeparator()).length - 1;
		int stoppingPoint = yytext().lastIndexOf(System.lineSeparator());
		int colSpacing = 1;
		if (stoppingPoint != -1) {
			colSpacing = yytext().length() - stoppingPoint - 1;
		}

		Pos.setpos();
		Pos.line += newLines;
		Pos.col = colSpacing;
	}

	"/" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.SLASH, new CSXToken(Pos));
	}

	"&&" {
		Pos.setpos();
		Pos.col +=2;
		return new Symbol(sym.CAND, new CSXToken(Pos));
	}

	[|][|] {
		Pos.setpos();
		Pos.col +=2;
		return new Symbol(sym.COR, new CSXToken(Pos));
	}

	"<=" {
		Pos.setpos();
		Pos.col +=2;
		return new Symbol(sym.LEQ, new CSXToken(Pos));
	}

	"<" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.LT, new CSXToken(Pos));
	}

	">=" {
		Pos.setpos();
		Pos.col +=2;
		return new Symbol(sym.GEQ, new CSXToken(Pos));
	}

	">" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.GT, new CSXToken(Pos));
	}

	"," {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.COMMA, new CSXToken(Pos));
	}

	"==" {
		Pos.setpos();
		Pos.col +=2;
		return new Symbol(sym.EQ, new CSXToken(Pos));
	}

	"=" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.ASG, new CSXToken(Pos));
	}

	"!=" {
		Pos.setpos();
		Pos.col +=2;
		return new Symbol(sym.NOTEQ, new CSXToken(Pos));
	}

	"!" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.NOT, new CSXToken(Pos));
	}

	":" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.COLON, new CSXToken(Pos));
	}

	";" {
		Pos.setpos();
		Pos.col +=1;
		return new Symbol(sym.SEMI, new CSXToken(Pos));
	}

	{INT} {
		Integer result = 0;
		String integerValue = yytext();
		boolean negative = false;

		//  Checks for negation character
		if (integerValue.charAt(0) == '~') {
			integerValue = integerValue.substring(1);
			negative = true;
		}
		try {
			result = Integer.parseInt(integerValue);
		} 
		// NumberFormatException is thrown by parseInt when overflow occurs
		catch (NumberFormatException ex) {
			//  Handles overflow by setting value to Integer.MAX_VALUE if positive value.
			//  Sets to Integer.MIN_VALUE if negative value.
			if (negative) {
				result = Integer.MIN_VALUE;
				System.out.println("\tError: Integer Overflow. Value set to Integer.MIN_VALUE");
			} 
			else {
				result = Integer.MAX_VALUE;
				System.out.println("\tError: Integer Overflow. Value set to Integer.MAX_VALUE");
			}
		}

		Pos.setpos(); 
		Pos.col += integerValue.length();

		if (negative) {
			Pos.col += 1;
		}
		
		return new Symbol(sym.INTLIT, new CSXIntLitToken(result, Pos));
	}

	{FLOAT}	{
		Float result = 0.0F;
		String floatValue = yytext();
		boolean negative = false;

		//  Checks for negation character
		if (floatValue.charAt(0) == '~') {
			floatValue = floatValue.substring(1);
			negative = true;
		}

		result = Float.parseFloat(floatValue);

		//  Similar overflow handling as integer literals.
		//  Difference being that Float does not throw an exception when overflow
		//  occurs. Instead, it sets the value to + or - infinity.

		if (result == Float.NEGATIVE_INFINITY || (result == Float.POSITIVE_INFINITY && negative)) {
			result = Float.MIN_VALUE;
			System.out.println("\tError: Float Overflow. Value set to Float.MIN_VALUE");
		} else if (result == Float.POSITIVE_INFINITY) {
			result = Float.MAX_VALUE;
			System.out.println("\tError: Float Overflow. Value set to Float.MAX_VALUE");
		}

		Pos.setpos(); 
		Pos.col += floatValue.length();

		if (negative) {
			Pos.col += 1;
		}
		
		return new Symbol(sym.FLOATLIT, new CSXFloatLitToken(result, Pos));
	}

	{BLANK} {
		Pos.col += 1;
	}

	{TAB} {
		Pos.col += yytext().length();
	}

	{EOL} {
		Pos.line += 1;
		Pos.col = 1;
	}

	{IDENTIFIER} {
		yybegin(IDSTATE);
		Pos.setpos();
		Pos.col += yytext().length();
		return new Symbol(sym.IDENTIFIER, new CSXIdentifierToken(yytext(), Pos));
	}

	{BAD_IDENTIFIER} {
		yybegin(IDSTATE);
		Pos.setpos();
		Pos.col += yytext().length();
		return new Symbol(sym.error, new CSXErrorToken("Illegal identifier: (" + yytext() + ")", Pos));
		
	}

	//  Changes to STRING state to check for string literals
	\" {
		str.setLength(0);
		yybegin(STRING);
	}

	//  Changes to CHAR state to check for character literals
	' {
		yybegin(CHAR);
	}

}

<STRING> {
	\" {
		yybegin(YYINITIAL);
		Pos.setpos();
		Pos.col += str.length() + 2;
		return new Symbol(sym.STRLIT, new CSXStringLitToken("\"" + str.toString() + "\"", Pos));
	}

	{PRINTABLE}+ {
		str.append(yytext());
	}

	\t	{
		str.append("\t");
	}
    
    {BLANK} {
    	str.append(" ");
    }

    \\\" {
    	str.append("\\\"");
    }

    {EOL} {
   		yybegin(YYINITIAL);
    	Pos.setpos();
		Pos.line += 1;
		Pos.col = 1;
    	return new Symbol(sym.error, new CSXErrorToken("Runaway string: \"" + str.toString(), Pos));
     }
}

<CHAR> {
	{CHARLIT}' {
		yybegin(YYINITIAL);
		Pos.setpos();
		Pos.col += yytext().length();
		return new Symbol(sym.CHARLIT, new CSXCharLitToken(yycharat(0), Pos));
	}
	\'' {
		yybegin(YYINITIAL);
		Pos.setpos();
		Pos.col += yytext().length();
		return new Symbol(sym.CHARLIT, new CSXCharLitToken('\'', Pos));
	}
	{EOL} {
		yybegin(YYINITIAL);
		Pos.setpos();
		Pos.line += 1;
		Pos.col = 1;
		return new Symbol(sym.error, new CSXErrorToken("Runaway character: '" + yytext(), Pos));
	}
	[^] {
		Pos.setpos();
		Pos.col += yytext().length();
		return new Symbol(sym.error, new CSXErrorToken("Invalid character literal: (" + yytext() + ")", Pos));
	}
}

<IDSTATE> {
	"++" {
		yybegin(YYINITIAL);
		Pos.setpos();
		Pos.col += 2;
		return new Symbol(sym.INC, new CSXToken(Pos));
	}

	"--" {
		yybegin(YYINITIAL);
		Pos.setpos();
		Pos.col += 2;
		return new Symbol(sym.DEC, new CSXToken(Pos));
	}

	[^] {
		yybegin(YYINITIAL);
	}
}

//  Fallback token - Invalid
[^] {
	Pos.setpos();
	Pos.col += 1;
	return new Symbol(sym.error, new CSXErrorToken("Invalid token (" + yytext() + ")", Pos));
}